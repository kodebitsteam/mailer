<?php
require_once  'checkPermissions.php';
require_once  'autoload.php';

include_once('lib/Campaigns.php');
$idCampaign = $_GET['id'];

$campaigns = new Campaigns();
$campaign = $campaigns->getCampaign($idCampaign);

$emails = $campaigns->getEmailsCampaign($idCampaign);


$template = $twig->loadTemplate('showCampaign.twig.html');
echo $template->render(array('campaign' => $campaign,'emails' => $emails));