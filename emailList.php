<?php
require_once  'checkPermissions.php';
require_once  'autoload.php';

include_once('lib/Emails.php');
$emails = new Emails();
$blacklist = $emails->getBlackList();

$error = '';
if(isset($_GET['error']))
    $error = $_GET['error'];

$template = $twig->loadTemplate('emailList.twig.html');
echo $template->render(array('blacklist' => $blacklist, 'error' => $error));