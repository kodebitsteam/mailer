<?php
require_once  'checkPermissions.php';
require_once  'autoload.php';

include_once('lib/Emails.php');
if(!empty($_POST)) {
    $emails = new Emails();
    //Check si existe
    $checkIfExists = $emails->checkEmail($_POST['email']);
    if(!$checkIfExists){
        $emails->addEmail($_POST);
        header('location: '.$base_url."admin/emails");
    }else{
        header('location: '.$base_url."admin/emails/add?error=1");
    }

}
