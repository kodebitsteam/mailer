<?php
require_once  'checkPermissions.php';
require_once  'autoload.php';

include_once('lib/Campaigns.php');
$campaignsModel = new Campaigns();
$campaigns = $campaignsModel->getCampaigns();

$template = $twig->loadTemplate('campaignsList.twig.html');
echo $template->render(array('campaigns' => $campaigns));