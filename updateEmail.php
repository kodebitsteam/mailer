<?php
require_once  'checkPermissions.php';

require_once  'autoload.php';

include_once('lib/Emails.php');
if(!empty($_POST))
{
    $emails = new Emails();
    $emails->updateEmail($_POST);

    header('location: '.$base_url."admin/emails");

}