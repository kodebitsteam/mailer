<?php
require_once  'checkPermissions.php';
require_once  'autoload.php';

$error = '';
if(isset($_GET['error']))
    $error = $_GET['error'];

$template = $twig->loadTemplate('addEmail.twig.html');
echo $template->render(array('error' => $error));