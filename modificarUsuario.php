<?php
require_once  'checkPermissions.php';
require_once  'autoload.php';

include_once('lib/Emails.php');
$idUser = $_GET['id'];

$emails = new Emails();
$email = $emails->getEmail($idUser);

$template = $twig->loadTemplate('modifyUser.twig.html');
echo $template->render(array('email' => $email[0]));