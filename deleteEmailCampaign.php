<?php
require_once  'checkPermissions.php';
require_once  'autoload.php';

include_once('lib/Campaigns.php');
$idEmail = $_GET['id'];
$idCampaign = $_GET['idCampaign'];

$campaigns = new Campaigns();
$campaign = $campaigns->getCampaign($idCampaign);
if($campaign[0]['status'] == 0)
    $campaigns->deleteEmailCampaign($idEmail,$idCampaign);

header('location: '.$base_url."admin/campañas/ver/$idCampaign");

