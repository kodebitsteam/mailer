<?php

include_once('Database.php');

class Emails
{

    /** @var Database */
    private $db;

    public function __construct($db = null)
    {
        $this->db = new Database();
    }

    public function getEmails($status){
        if($status == 0){
            $where = "(status = 0 or status = 2)";
        }else{
            $where = "status = 1";
        }
        $sql = "SELECT *
            FROM emails 
            where $where";

        $result = $this->db->select($sql);

        if ($result) {
            return $result;
        }else{
            return null;
        }
    }

    public function getEmail($id){
        $sql = "SELECT *
            FROM emails 
            where id = $id
            ";

        $result = $this->db->select($sql);

        if ($result) {
            return $result;
        }else{
            return null;
        }
    }

    public function checkUnsubscribe($token,$id){
        $sql = "SELECT email
            FROM campaigns_emails 
            where id=$id";

        $result = $this->db->select($sql);
        if ($result) {
            if(md5($result[0]['email']."guaridamailer2019") == $token){
                    $sql2 = "SELECT *
                    FROM blacklist_emails 
                    where email ='".$result[0]['email']."'";
                    $result2 = $this->db->select($sql2);
                    if ($result2) {
                        return false;
                    }else{
                        return true;
                    }

            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public function checkIfNotUnsubscribe($email){
        $sql = "SELECT *
            FROM blacklist_emails 
            where email ='".$email."'";

        $result = $this->db->select($sql);

        if ($result) {
            return false;
        }else{
            return true;
        }
    }

    public function checkEmail($email){
        $sql = "SELECT *
            FROM emails 
            where email ='".$email."'";

        $result = $this->db->select($sql);

        if ($result) {
            return true;
        }else{
            return false;
        }
    }

    public function unSubscribe($id){

        $sql = "SELECT *
            FROM campaigns_emails 
            where id =$id";

        $result = $this->db->select($sql);
        if($result){
            $sql2 = 'INSERT INTO blacklist_emails (email) VALUES ("'.$result[0]['email'].'")';
            $result2 = $this->db->query($sql2);
        }
    }

    public function addEmailToCampaign($data){
        $sql = 'INSERT INTO campaigns_emails (id_campaign,email) VALUES ("'.$data['id'].'","'.$data['email'].'")';
        $result = $this->db->query($sql);
        if ($result) {
            return $result;
        }else{
            return null;
        }
    }

    public function checkEmailRepeatAtCampaign($email,$id){
        $sql = "SELECT *
            FROM campaigns_emails 
            where email ='".$email."' and id_campaign=$id";

        $result = $this->db->select($sql);

        if ($result) {
            return true;
        }else{
            return false;
        }
    }

    public function getBlackList(){
        $sql = "SELECT *
            FROM blacklist_emails ORDER BY created_at desc";

        $result = $this->db->select($sql);
        if ($result) {
            return $result;
        }else{
            return null;
        }
    }
}