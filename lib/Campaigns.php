<?php
include_once('Database.php');

class Campaigns
{
    /** @var Database */
    private $db;

    public function __construct($db = null)
    {
        $this->db = new Database();
    }

    public function getCampaigns(){
        $sql = "SELECT *
            FROM campaigns 
            order by date desc
            ";

        $result = $this->db->select($sql);

        if ($result) {
            return $result;
        }else{
            return null;
        }
    }

    public function getCampaign($id){
        $sql = 'SELECT *
            FROM campaigns 
            where id='.$id;

        $result = $this->db->select($sql);

        if ($result) {
            return $result[0];
        }else{
            return null;
        }
    }


    public function addCampaign($data){
        $date = explode("-",$data['date']);
        $dateFormat = $date[2]."-".$date[1]."-".$date[0];
        $sql = 'INSERT INTO campaigns (name,template,date) VALUES ("'.$data['name'].'","'.$data['template'].'","'.$dateFormat.'")';

        $result = $this->db->query($sql);

        if ($result) {
            return $result;
        }else{
            return null;
        }
    }

    public function deleteCampaign($id){

        $sql = 'DELETE FROM campaigns where id='.$id;

        $result = $this->db->query($sql);

        $sql2 = 'DELETE FROM campaigns_emails where id_campaign='.$id;

        $result2 = $this->db->query($sql2);

        if ($result) {
            return $result;
        }else{
            return null;
        }
    }

    public function updateCampaign($data){
        $date = explode("-",$data['date']);
        $dateFormat = $date[2]."-".$date[1]."-".$date[0];
        $sql = "UPDATE 
            campaigns set name='".$data["name"]."', template='".$data['template']."', date='".$dateFormat."', status='".$data['status']."'
            where id =".$data['id'];

        $result = $this->db->query($sql);

        if ($result) {
            return $result;
        }else{
            return null;
        }
    }

    public function updateStatus($id,$status){
        $sql = "UPDATE 
            campaigns set status='".$status."'
            where id =".$id;

        $result = $this->db->query($sql);

        if ($result) {
            return $result;
        }else{
            return null;
        }
    }

    public function getEmailsCampaign($id){
        $sql = 'SELECT *
            FROM campaigns_emails 
            where id_campaign='.$id;

        $result = $this->db->select($sql);

        if ($result) {
            return $result;
        }else{
            return null;
        }
    }

    public function deleteEmailCampaign($idEmail,$idCampaign){
        $sql = 'DELETE FROM campaigns_emails where id='.$idEmail.' and id_campaign = '.$idCampaign;

        $result = $this->db->query($sql);

        if ($result) {
            return $result;
        }else{
            return null;
        }
    }

}