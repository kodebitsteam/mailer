<?php
require_once  'checkPermissions.php';
require_once  'autoload.php';

include_once('lib/Emails.php');
if(isset($_GET['t']) and isset($_GET['p'])) {
    $emails = new Emails();
    $token = $_GET['t'];
    $id = $_GET['p'];
    $checkUnsubscribe = $emails->checkUnsubscribe($token,$id);
    if($checkUnsubscribe){
        $emails->unSubscribe($id);
        header("location: http://www.tuseguroempresa.es/?unsuscribe=true");
    }else{
        header("location: http://www.tuseguroempresa.es/");
    }

}else{
    header("location: http://www.tuseguroempresa.es/");
}
