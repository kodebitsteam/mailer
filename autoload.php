<?php
require_once  'vendor/autoload.php';

$base_url = "/mailer/";

Twig_Autoloader::register();
$loader = new Twig_Loader_Filesystem( 'templates');

$twig = new Twig_Environment($loader, array(
            'cache ' =>  'cache ',
        'debug ' =>  'true '
));
$twig->addGlobal('base_url',$base_url);

$twig->addExtension(new Twig_Extension_Debug());

$mail = new PHPMailer(true);
$mail->IsSMTP();
$mail->SMTPOptions = array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
    )
);
$mail->Host = "smtp.strato.com";  // specify main and backup server
$mail->SMTPSecure = 'tls';
$mail->SMTPAuth   = true;  // turn on SMTP authentication
$mail->Username = "webmaster@tuseguroempresa.es";  // SMTP username
$mail->Password = "qaaqb1Asdi++1a"; // SMTP password

$mail->Port = 587;

$mail->From = "Webmaster@tuseguroempresa.es";
$mail->FromName = "SRF Seguros empresas";