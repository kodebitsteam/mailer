<?php
require_once  'autoload.php';

include_once('lib/Campaigns.php');
include_once('lib/Emails.php');

$idCampaign = $_GET['id'];
if($idCampaign){
    $campaigns = new Campaigns();
    $campaign = $campaigns->getCampaign($idCampaign);
    $emails = new Emails();

    $mailer = new \RauweBieten\TwigMailer\TwigMailer($mail, $twig);

    $emailsList = $campaigns->getEmailsCampaign($idCampaign);

    foreach ($emailsList as $email){
        //Comprobamos si por lo que sea esta en lista negra..
        if($emails->checkIfNotUnsubscribe($email['email'])) {
            $token = md5($email['email'] . "guaridamailer2019");
            $mailer->create('newsletters/' . $campaign['template'] . '.twig.html', [
                'id' => $email['id'],
                'campaign' => $campaign,
                'token' => $token
            ]);
        }
        $mailer->getPhpMailer()->clearAddresses();
        $mailer->getPhpMailer()->addAddress($email['email']);
        $mailer->send();
    }

    $campaigns->updateStatus($idCampaign,1);

    header('location: '.$base_url."admin/campañas");

}

