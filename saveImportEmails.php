<?php
require_once  'checkPermissions.php';
require_once  'autoload.php';
include("lib/excel-reader.php");
include_once('lib/Emails.php');
if($_FILES['archivo']['name'] != ''){

    $emails = new Emails();

    $fichero_subido =  basename($_FILES['archivo']['name']);
    move_uploaded_file($_FILES['archivo']['tmp_name'], $fichero_subido);
    if ( $xlsx = SimpleXLSX::parse($fichero_subido)) {
        foreach ($xlsx->rows() as $key => $user) {

                $email = $user[0];
                if($email){
                    $idCampaign = $_POST['id'];
                    //Comprobamos si existe el email..
                    if($emails->checkIfNotUnsubscribe($email)){
                        if(!$emails->checkEmailRepeatAtCampaign($email,$idCampaign)){
                            $data['email'] = $email;
                            $data['id'] = $idCampaign;
                            $emails->addEmailToCampaign($data);
                        }

                    }
                }


        }
    }else{
        echo SimpleXLSX::parse_error();
    }
    unlink($fichero_subido);
}

header('location: '.$base_url."admin/campañas/ver/$idCampaign");
