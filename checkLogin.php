<?php

require_once  'autoload.php';

include_once('lib/User.php');
if(!empty($_POST))
{
    $users = new User();
    $user = $users->checkLogin($_POST['user'],$_POST['password']);

    if($user > 0) {
        session_start();
        $_SESSION['role'] = $user[0]['role'];
        $_SESSION['id'] = $user[0]['id'];

        header('location: '.$base_url."admin/campañas");
    } else {
        header("location: ../?error=1");
    }
}

